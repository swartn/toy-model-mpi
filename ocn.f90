program ocn
    use mpi
    use com_cpl, only: define_group

    implicit none
    integer ierror, size, rank, i, ocn_local_comm, color, key
    character (len=128) message
    logical, parameter :: do_chatter = .FALSE.    

    
    call MPI_INIT(ierror)
    call define_group('ocn', ocn_local_comm)

    call MPI_COMM_SIZE(ocn_local_comm, size, ierror)
    call MPI_COMM_RANK(ocn_local_comm, rank, ierror)
  
    write(*,*) 'OCN :', rank, ' of ', size  
    call flush(6)

    ! Do an ocean inter communication
    if ( do_chatter )  then
        if(rank == 0) then
            message = "We ALL know the ocean is deep"
            write(6,*) "Ocean lead communicates: ", trim(message)
            call flush(6)
            do i = 1, size -1
               call MPI_SEND(message, 128, MPI_CHAR, i, 1, ocn_local_comm, ierror)
            end do
        else
            call MPI_RECV(message, 128, MPI_CHAR, 0, 1, ocn_local_comm, MPI_STATUS_IGNORE, ierror)
            write(6,*) "Ocean rank", rank, " of ",  size," gets the command: ", trim(message)
            call flush(6)
        end if
    endif

    call MPI_FINALIZE(ierror)

end program ocn