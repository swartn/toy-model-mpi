FC=ftn
FFLAGS=-traceback -O0 -g 
OBJ=atm.exe cpl.exe ocn.exe

all : $(OBJ)

%.mod : %.f90
	$(FC) -c $^

%.o : %.f90 com_cpl.f90 com_cpl.mod
	$(FC)  -o $@ -c $< $(FFLAGS)

%.exe : %.o com_cpl.o 
	$(FC)  -o $@ $^ $(FFLAGS)

clean: #cleans all the old compilation files
	@rm -f *.mod *.o atm.exe ocn.exe cpl.exe